package tennis;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;

public class Tennis05 {
  public static void main(String[] args) {
    List<Club> table = Arrays.asList(
    		new Club(1, "Slammers", 54, 19, 2, 8, 564, 850, 125, 55, 11,
    	            5, 100, 88),
    	        new Club(2, "Elemex Club", 54, 19, 1, 5, 425, 741, 111, 12, 53, 2, 7, 80),
    	        new Club(3, "White Strings", 54, 30, 2, 8, 751, 422, 22, 66, 44, 6,
    	            3, 86),
    	        new Club(4, "Seniotitas", 54, 10, 2, 13, 774, 528, 100, 94, 26, 4, 4, 18),
    	        new Club(5, "Short Queens", 54, 10, 1, 7, 555, 216, 454, 50, 16, 6, 9 ,
    	            77),
    	        new Club(6, "Net Gamers", 54, 10, 3, 9, 400, 317, 111, 47, 14, 6,4,21),
    	        new Club(7, "Laser Short", 54, 10, 1, 12, 697, 182, 17, 22, 14, 6, 4,
    	            14),
    	        new Club(8, "Topspinners", 54, 19, 1, 11, 551, 104, -20, 35, 80, 3, 4,
    	            69),
    	        new Club(9, "Smart Aces", 54, 7, 2, 62, 233, 465, -89, 63, 91, 5, 7, 75),
    	        new Club(10, "Court Glazer", 54, 45, 2, 14, 132, 875, -206, 16, 47, 5, 7,
    	            75),
    	        new Club(11, "Victoria Secret", 54, 5, 2, 10, 555, 620, -10, 34, 51,
    	            3, 6, 45),
    	        new Club(12, "BigBubblers", 54, 1, 1, 55, 663, 1255, -891, 19, 547, 2,
    	            1,2));


    System.out.println("Sorted by Comparator in Club class");
    table.stream().sorted().forEach(System.out::println);

    System.out.println();
    System.out.println("Sorted by lambda");
    table.stream()
         .sorted((c1, c2) -> 
            ((Integer) c1.getPointsAgainst()).compareTo(c2.getPointsAgainst()))
         .forEach(System.out::println);

  }

}
